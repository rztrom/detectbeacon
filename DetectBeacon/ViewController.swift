//
//  ViewController.swift
//  DetectBeacon
//
//  Created by Anders Zetterström on 2015-10-08.
//  Copyright © 2015 Anders Zetterström. All rights reserved.
//

import UIKit
import CoreLocation
import AVFoundation
import CoreBluetooth

class ViewController: UIViewController, CLLocationManagerDelegate{
    
    @IBOutlet weak var majorLabel: UILabel!
    @IBOutlet weak var minorLabel: UILabel!
    @IBOutlet weak var rssiLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var accuracyLabel: UILabel!
    @IBOutlet weak var wibrickImg: UIImageView!
    
    let soundName : String = "pindropping"
    let soundType : String = "mp3"
    
    var locationManager : CLLocationManager!
    var audioPlayer: AVAudioPlayer = AVAudioPlayer()
    var counter = 0;
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
        
        view.backgroundColor = UIColor.grayColor()
        prepareAudios()
        
    }
    
    func prepareAudios(){
      let audioFilePath = NSBundle.mainBundle().pathForResource(soundName, ofType: soundType)
        
        if audioFilePath != nil {
            let audioFileUrl = NSURL.fileURLWithPath(audioFilePath!)
            var _: NSError
            do{
               try audioPlayer = AVAudioPlayer(contentsOfURL: audioFileUrl, fileTypeHint:nil)
            }catch {
                print("Error on audioplayer")
            }
            audioPlayer.prepareToPlay()
        }
        
    }

    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if(status == CLAuthorizationStatus.AuthorizedAlways){
            if(CLLocationManager.isMonitoringAvailableForClass(CLBeaconRegion.self)) {
                if CLLocationManager.isRangingAvailable() {
                    startScanning()
                }
            }
        }
    }
    
    func startScanning(){
       let uuid = NSUUID(UUIDString: "E2620F38-8013-4E2E-9243-CF23B1D00D0B")
        
        let beaconRegion = CLBeaconRegion(proximityUUID: uuid!, identifier: "WiBrick")
        
        locationManager.startMonitoringForRegion(beaconRegion)
        locationManager.startRangingBeaconsInRegion(beaconRegion)
        locationManager.accessibilityActivate()
    }
    
    func updateDistance(distance: CLProximity){
        UIView.animateWithDuration(1) { [unowned self] in
            switch distance {
            case .Unknown:
                self.view.backgroundColor = UIColor.grayColor()
                let unknownStr = " - - "
                self.majorLabel.text = unknownStr
                self.minorLabel.text = unknownStr
                self.rssiLabel.text = unknownStr
                self.distanceLabel.text = unknownStr
                self.accuracyLabel.text = unknownStr
                self.wibrickImg.alpha = 0.3
                
            case .Far:
                self.view.backgroundColor = UIColor(red: 0/255, green: 71/255, blue: 64/255, alpha: 1)
                self.distanceLabel.text = "Far"
                self.wibrickImg.alpha = 0.5
                if self.counter > 6 {
                    self.audioPlayer.play()
                    self.counter = 0
                }

            case .Near:
                self.view.backgroundColor = UIColor(red: 15/255, green: 147/255, blue: 133/255, alpha: 1)
                self.distanceLabel.text = "Near"
                self.wibrickImg.alpha = 0.8
                if self.counter > 3 {
                self.audioPlayer.play()
                    self.counter = 0
                }
            case .Immediate:
                self.view.backgroundColor = UIColor(red: 105/255, green: 237/255, blue: 223/255, alpha: 1)
                self.distanceLabel.text = "Right here"
                self.audioPlayer.play()
            }
            self.counter++
        }
    }
    
    
    func locationManager(manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], inRegion region: CLBeaconRegion) {
        
        if beacons.count > 0 {
            let beacon = beacons.first
            let accuracyStr = String(format: "%0.2f", (beacon!.accuracy))
            let majorStr = String((beacon!.major.integerValue))
            let minorStr = String((beacon!.minor.integerValue))
            let rssiStr = String((beacon!.rssi))
            
            print(beacon)
            
            majorLabel.text = "Major: \(majorStr)"
            minorLabel.text = "Minor: \(minorStr)"
            rssiLabel.text = "RSSI: \(rssiStr)"
            accuracyLabel.text = "About \(accuracyStr) meters away"
            updateDistance((beacon!.proximity))
            
        } else {
            print(beacons)
            updateDistance(.Unknown)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

